---
layout: handbook-page-toc
title: "CI/CD UX Team"
description: "CI/CD UX team works to ensure the best experience for users of all knowledge levels to successfully apply continuous methods with no 3rd-party application or integration"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Hello! We're the GitLab CI/CD UX team. We're comprised of three stage groups that support designing and building the GitLab DevOps product: [Verify](/direction/ops/#verify), [Package](/direction/ops/#package), and [Release](/direction/ops/#release).

Our design mission is to bring simple, clean ways to make GitLab the tool of choice for deploying where, when, and how users want.

We work to ensure the best user experience for our users of all knowledge levels, allowing them to successfully apply the continuous methods (Continuous Integration, Delivery, and Deployment) to their software with no third-party application or integration needed.

### Our UX pages

* [Verify UX](/handbook/engineering/ux/stage-group-ux-strategy/verify/)
  * [Pipeline Execution UX](/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/)
  * Pipeline Authoring UX
  * Testing & Runner UX
* [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)
* [Package UX](/handbook/engineering/ux/stage-group-ux-strategy/package/)

### Common Links

| **Our team** | Meet all [CI/CD UX team members](/company/team/?department=ci-cd-ux-team) |
| **GitLab.com** | [/ci-cd-ux](https://gitlab.com/gitlab-org/ci-cd-ux) |
| **Slack Channel** | [`#ux_ci-cd`](https://gitlab.slack.com/app_redirect?channel=ux_ci-cd) |
| **Youtube playlist** | [CI/CD UX Team meetings](https://www.youtube.com/watch?v=ViEs54I5lHE&list=PL05JrBw4t0Kpap0GkV0SSuGnPhCM8jrAv) |

## How we work

We follow the workflows outlined in the [UX section](/handbook/engineering/ux/how-we-work/) of the handbook. In addition,

* we lay down [team OKRs](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=OKR) to execute our UX Department strategy.
* we use our own [UX Definition of Done (DoD)](#ux-definition-of-done-dod).
* we participate in the [monthly milestone kick off](/handbook/engineering/ux/ux-department-workflow/#milestone-kickoff). Together with Product Managers, we record a video with a focus on the user experience, front-end (user interface design) and research innitiatives.
    * [View all Milestone Kickoff playlists](https://about.gitlab.com/direction/kickoff/#ops-section)
* we perform synchronous and asyncrhonous [design reviews](/handbook/engineering/ux/ux-resources/#synchronous-design-reviews).
    * [Watch our syncrhonous design reviews on Youtube](https://www.youtube.com/watch?v=AYO97sRry4E&list=PL05JrBw4t0Kpnb8RDztlfpryAYip1OMwb)

### UX Definition of Done (UX DoD)

The UX DoD lists the activities a Product Designer is responsible for in the [Product Development Flow](/handbook/product-development-flow/). This list can be applied to an epic/issue, and serves as a tool for describing and tracking the expected UX deliverables, objectives, and the approval process.

To keep the process efficient, depending on the scope of a problem, a Product Designer might not need to check off all of the items in the UX DoD while working through the Product Development Flow.

You can add the following checklist to an issue description to help illustrate the "completeness" of a design proposal:

```

### UX Definition of Done

1️⃣ **VALIDATION TRACK**

**Problem Validation Phase**
- [ ] Problem is well understood and has been validated
- [ ] JTBD is well understood and has been validated
- [ ] PM has communicated the opportunity canvas to stable counterparts and group stakeholders, including the Product Designer and Product Design Manager

**Design Phase**
- [ ] Document the JTBD and UX goal in the issue/epic description
- [ ] Explore multiple different approaches as a team
- [ ] Discuss the technical implications with Engineering
  - [ ] Identify any potential cross-team dependencies and include the DRIs in the discussions
- [ ] Identify a small set of options to validate
    - [ ] Document the user story(ies) for the MVC
    - [ ] Consider edge cases for each user story
    - [ ] Create prototypes or mockups for each user story
- [ ] [Pajamas component lifecyle](https://design.gitlab.com/get-started/lifecycle)
    - [ ] Identify component design or pattern update/creation
    - [ ] Discuss the technical implications with Engineering
    - [ ] Pajamas issue is created (within the scope of the MVC)
- [ ] Update issues/epic descriptions
    - [ ] The appropriate [labels](https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/#how-we-use-labels) were applied
      - [ ] If changes involve copy, add the ~"Technical Writing" and ~"UI text" labels
- [ ] Proposed solution(s) identified and documented in the issue/epic description

**Solution Validation Phase**
- [ ] Validate the solution to increase confidence in the proposed solution
- [ ] Document the solution validation learnings
- [ ] Product Designer has communicated the solution validation learnings to stable counterparts and group stakeholders, including the Product Design Manager
- [ ] Update the MVC proposal with relevant insights from the solution validation
  - [ ] Discuss the technical implications with Engineering
  - [ ] Update issue/epic description to contain or link to the findings

2️⃣ **BUILD TRACK**

[**Plan Phase**](https://about.gitlab.com/handbook/product-development-flow/#build-phase-1-plan)
- [ ] Proposal is ready to be broken down and prioritized by PM for development

**Develop & Test Phase**
- [ ] Product Designer reviewed MRs that include user-facing changes, as per the [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html)
  - [ ] UX Debt issues have been identified and assigned to a milestone

```

## Our strategy

Our strategy is all about making sure that even complex delivery flows become an effortless part of everyone's primary way of working. We work closely with Engineering, Product Management, User Research, Technical Writing, and Product Marketing to make sure we have all the support in uncovering user needs and work to solve them together. Some of our strategic initiatives are:

* [Jobs to be Done framework](/handbook/engineering/ux/jobs-to-be-done/)
* [UX Scorecards and recommendations](/handbook/engineering/ux/ux-scorecards/) 
* [Opportunity canvas](/handbook/product-development-flow/#opportunity-canvas) 
* [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards/)
* Stakeholder, user, and customer interviews
* Competitive analysis

Learn about our [high level UX strategy](/handbook/engineering/ux/#our-strategy).

Visit the [Ops Product Section Direction](/direction/ops/) to read about the product strategy.

### Our strategic counterparts

We have business goals we are shooting for all the time. To understand how we can measure success in the CI/CD area, we collect insights from our strategic counterparts: [Product Marketing Managers](/handbook/marketing/strategic-marketing/pmmteam/), [Analyst Relations](/handbook/marketing/strategic-marketing/analyst-relations/), and [Customer Success](/handbook/customer-success/). 

Below you can find information about the all designated counterparts we work with:

* [Verify stage counterparts](/handbook/product/categories/#verify-stage)
* [Package stage counterparts](/handbook/product/categories/#package-stage)
* [Release stage counterparts](/handbook/product/categories/#release-stage)

## Our team meetings

### CI/CD UX Team Meeting

We meet bi-weekly as a team to discuss our work, processes, talk about User Research activities, share knowledge, and raise questions to each other. We are also using session for team retrospectives, as well as sharing useful resources around design and DevOps domains.

* Watch the [CI/CD UX Team Meeting videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqkrzZyJrJSEWNyiL_5x7an) on Unfiltered

### Ops Cross-Stage ThinkBIG!

The purpose of Ops Cross-Stage [ThinkBIG! meeting](/handbook/engineering/ux/thinkbig/) is to discuss the vision, product roadmap, user research, and design work related to the Cross-Stage Ops experience at GitLab.

* Watch the [Ops Cross-Stage ThinkBIG! videos](https://www.youtube.com/playlist?list=PL05JrBw4t0KqFdx966BWkg9-RwXyBDq1k) on Unfiltered

## Personal Growth Day (Pilot)

To ensure we continue to grow and sharpen our skills as designers, we are running a pilot where we dedicate one day per milestone to our individual growth. Our first team growth day is `2021-02-25`. See the [pilot issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/26). 

On our dedicated personal growth day: 
* Our external syncs are cancelled or moved
* We don't work on product design work for the day
* Our team syncs are dedicated to sharing resources, providing feedback on growth plans, etc.

### Growth Resources

* [Career Development](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/handbook/people-group/learning-and-development/career-development)
* [Learning & Development](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/handbook/people-group/learning-and-development)
* [Learning Initiatives](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/handbook/people-group/learning-and-development/learning-initiatives)
