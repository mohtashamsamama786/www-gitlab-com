---
layout: handbook-page-toc
title: Package Group - Container Registry
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The goal of this page is to document specific processes and tools for the [GitLab Container Registry](https://gitlab.com/gitlab-org/container-registry) project.

## Historical Context

In milestone 8.8, GitLab launched the MVC of the Container Registry. This feature integrated the [Docker Distribution registry](https://docs.docker.com/registry/) into GitLab so that any GitLab user can have a space to publish and share container images. 

But there was an inherent problem with Docker Distribution. When you delete a container image tag, it's not actually deleted from storage. Instead, it is marked for deletion, and it will only be removed from storage when garbage collection is run. The problem is that the registry must be set to read-only mode or downtime to run garbage collection. Given the scale and SLAs of GitLab.com, it has not been possible to schedule downtime. 

Fast forward many milestones, and the problem has continued to grow linearly. The GitLab.com registry consumes petabytes of storage which costs tens of thousands of dollars every month. 

### What's been done so far?

Two years ago, the Package group and GitLab Staff engineers had a lengthy [discussion](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/62885) about how to resolve this problem and to deliver a [lovable product](https://gitlab.com/groups/gitlab-org/-/epics/5136). We evaluated several options, including:

- Integrating with another service like Harbor or Docker Trusted registry
- Building our in-house registry
- Forking Docker Distribution and iterating on that code

In the end, the decision was made to fork Docker distribution and make the requisite updates to add support for online garage collection. 

Along the way, we made several other changes and modifications, which were all targeted to help GitLab and its customers tackle this storage problem. 

First, we optimized the existing garbage collection algorithms for [GCS](https://gitlab.com/groups/gitlab-org/-/epics/2552) and [S3](https://gitlab.com/groups/gitlab-org/-/epics/2553), so that large enterprises could be unblocked from running garbage collection, even if it required downtime. This helped improve the performance of the algorithm by 90+ percent. 

We also added programmatic [cleanup policies](https://docs.gitlab.com/ee/user/packages/container_registry/#cleanup-policy) to help customers automatically remove (even if they were not deleted from storage) old unused images. 

Fast forward a bit, and, as a team, we've evaluated and iterated on designs for the implementation of online garbage collection and several plans for the migration of one registry to the next. The [epic](https://gitlab.com/groups/gitlab-org/-/epics/6405) details the work required to deploy the new metadata database to production and migrate all new and existing repositories to use the feature. 

### Why we are excited

The metadata database is not just about online garbage collection. It unblocks a whole new set of potential features and capabilities that will help our customers to manage and deploy their software reliably. For example, it unblocks some much-needed updates for the [API](https://gitlab.com/groups/gitlab-org/-/epics/5683) so that we can support a more [robust user interface](https://gitlab.com/groups/gitlab-org/-/epics/3211), add enterprise features like [image signing](https://gitlab.com/gitlab-org/container-registry/-/issues/83) and [protection](https://gitlab.com/gitlab-org/gitlab/-/issues/18984) and provide more stability and reliability. 

By forking the project, we continued improving the application and implementing several bug fixes, performance improvements, and additional features that were not available or accepted upstream. Consequently, due to the rate of changes and how the codebases diverged, we decided to detach from upstream in [June 2020](https://gitlab.com/gitlab-org/container-registry/-/issues/139). Since then, we have been evolving the project in isolation. We continue to source changes from upstream whenever necessary (mostly security fixes), but these must be merged manually.

In [February 2021](https://www.docker.com/blog/donating-docker-distribution-to-the-cncf/), Docker decided to donate Docker Distribution to the Cloud Native Computing Foundation (CNCF) to revive the project. The project was then renamed to [Distribution](github.com/distribution/distribution). Some of the GitLab Container Registry maintainers are now maintainers of the CNCF project as well.

Although some of our engineers contribute to the upstream Distribution project, the codebases diverged beyond reconciliation. Therefore, there is no intention to reunite with upstream at any point in time. Some of the features we have been working on, such as the metadata database and online garbage collection (more on that later), required a significant platform re-architecture. Additionally, we will continue to tailor the GitLab Container Registry to suit the needs of GitLab the product.

## Documentation

+The documentation is currently scattered across multiple places, namely this handbook page, [docs.gitlab.com](http://docs.gitlab.com/), the [project repository](gitlab.com/gitlab-org/container-registry), and the upstream [Docker documentation](https://docs.docker.com/registry/). This is a [known issue](https://gitlab.com/groups/gitlab-org/-/epics/5965) and something we intend to address.

### Development

The following documentation is especially relevant for engineers working with the Container Registry:

- [Differences from Upstream](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#differences-from-upstream) - This is where we keep track of differences between the GitLab Container Registry and the upstream CNCF Distribution. All user-facing changes must be documented.
- [Releases](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#releases) - How we manage and cut new releases.
- [Contributing](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#contributing) - We stay as close as possible to the general GitLab development guidelines but enforce stricter rules whenever appropriate. Here is where we document those specific contributing processes.
- [Development Guidelines](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#development) - Links for specific development documentation, ranging from setup instructions to general GitLab development guidelines extensions.
- [Technical Documentation](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#technical-documentation) - Documentation about specific components or features of the application. This includes components and features inherited from upstream (with no documentation available elsewhere) and new ones.
- [Configuration](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/configuration.md) - Documentation about the available application settings. The [upstream configuration documentation](https://docs.docker.com/registry/configuration/) from Docker was the base for this, but since then, we have added, deprecated, and changed multiple configurations.
- [Storage Drivers](https://docs.docker.com/registry/storage-drivers/) - The documentation for the storage drivers. This is only available upstream. Whenever we add or change a storage driver settings, we document it [here](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/README.md#configuration).
- [Notifications](https://docs.docker.com/registry/notifications/) - Documentation about the asynchronous webhook notifications feature.
- [Authentication](https://docs.docker.com/registry/spec/auth/) - All about the authentication specification implemented by the Container Registry and supported by GitLab Rails (the authentication provider).

### Administration

The following links are related to administrative tasks, mainly for self-managed instances:

- [Omnibus and Source Installs](https://docs.gitlab.com/ee/administration/packages/container_registry.html)
- [Helm Chart Installs](https://docs.gitlab.com/charts/)

### Architecture

Documentation about the current architecture or any significant changes to it. The latter usually come in the form of an [Architecture Blueprint](https://about.gitlab.com/handbook/engineering/architecture/workflow/):

* [Container Registry Metadata Database](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database/) - Blueprint for the architecture change to move metadata from the storage backend into a database.

## Observability

### Metrics and Dashboards

The registry exports metrics about all its components (HTTP API, storage, database and online GC) to Prometheus. These metrics are geared towards operational metrics only. Due to cardinality limitations in Prometheus, metrics that require variable labels such as repository and image identifiers are exposed through the application logs.

For GitLab.com, the container registry has multiple dashboards in Grafana and Kibana, listed below. Like for any other GitLab application, observability is critical. Take your time to go through all dashboards and becoming familiar with their layout and functionalities.

All the underlying metrics for the Grafana and Kibana dashboards are also available on self-managed installs.

#### Grafana

* [Overview](https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1) - Main service dashboard. Provides an overview of the Service Level Indicators (SLI). The information is available for all the service components.
* [Application Detail](https://dashboards.gitlab.net/d/registry-app/registry-application-detail?orgId=1) - Detailed information about the application metrics. Provides insight about the HTTP API and the hosts resources usage.
* [Storage Detail](https://dashboards.gitlab.net/d/registry-storage/registry-storage-detail?orgId=1) - Consolidated information about the registry storage backend - Google Cloud Storage (GCS).
* [Database Detail](https://dashboards.gitlab.net/d/registry-database/registry-database-detail?orgId=1) - Fine grain metrics about the metadata database.
* [Garbage Collection Detail](https://dashboards.gitlab.net/d/registry-gc/registry-garbage-collection-detail?orgId=1) - Extensive metrics related to the online GC feature.
* [Migration Detail](https://dashboards.gitlab.net/d/registry-migration/registry-registry-migration-detail?orgId=1) - Temporary dashboard to support the ongoing GitLab.com deployment and migration ([gitlab-org&5523](https://gitlab.com/groups/gitlab-org/-/epics/5523)).
* [PgBouncer](https://dashboards.gitlab.net/d/pgbouncer-registry-main/pgbouncer-registry-overview) - Metrics for the PgBouncer nodes in front of the registry PostgreSQL database cluster.

#### Kibana

* [Main](https://log.gprd.gitlab.net/goto/7ac27e1df0f8fca57ad8ceb383696821) - Overview of several registry metrics.
* [Blob Downloads](https://log.gprd.gitlab.net/goto/05f1f0f38ca1af421d70691d6e80c069) - Provides additional insight about downloads at the top-level namespace and repository levels.

### Logs

The container registry exposes structured access and application logs. For GitLab.com, these logs can be found in Kibana:

* [Non-production](https://nonprod-log.gitlab.net/goto/f3fbccdb9dea6805ff5bbf1e0144a04e): Logs for the development, staging, and pre-production environments.
* [Production](https://log.gprd.gitlab.net/goto/7dc6f73d5dd4cc4bebcd4af3b767cae4): Logs for the production environment.
